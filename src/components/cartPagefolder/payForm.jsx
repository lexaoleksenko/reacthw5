import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, Form, useField} from 'formik';
import * as Yup from 'yup'
import { buyItem } from '../../redux/actions';

const MyTextInput = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
    <>
        <label htmlFor={props.id || props.name} style ={{color: meta.touched && meta.error ? "red" : "black"}}>{label}</label>
        <input className= {meta.touched && meta.error ? "form__input form__input_err"  : "form__input"} {...field} {...props} />
        {meta.touched && meta.error ? (<div className="form__input_errtxt">{meta.error}</div>) : null}
    </>
    );
    };



function PayForm(){
    const dispatch = useDispatch();
    const cartItems = useSelector(state=>{
        const {cartReducer} = state
        return cartReducer.itemsCart
    })

    return(
        <Formik
        initialValues={{ FirstName: '', LastName: '', Age: '', Adress: '', Phone: '',}}
        validationSchema = {Yup.object({
            FirstName: Yup.string()
                .max(15, '*Must be 15 characters or less')
                .required('Required'),
            LastName: Yup.string()
                .max(20, '*Must be 20 characters or less')
                .required('Required'),
            Age: Yup.number()
                .min(16, '*Age is under 16')
                .required('Required'),
            Adress: Yup.string()
                .max(30, '*Must be 30 characters or less')
                .required('Required'),
            Phone: Yup.string()
                .max(13, '*Must be 13 characters or less')
                .required('Required')})}
        onSubmit={(values, { setSubmitting }) => {
                console.log(JSON.stringify(values, null, 2));
                setSubmitting(false);
                console.log(cartItems);
                dispatch(buyItem());
                }}
        >
            <Form className = 'form'>
            <span>Order form</span>

            <MyTextInput 
                label="First Name"
                name="FirstName"
                type="text"
                placeholder="Sherlock"
            />

            <MyTextInput 
                label="LastName"
                name="LastName"
                type="text"
                placeholder="Holmes"
            />

            <MyTextInput 
                label="Your Age"
                name="Age"
                type="text"
                placeholder="30"
            />

            <MyTextInput 
                label="Your Adress"
                name="Adress"
                type="text"
                placeholder="221B Baker Street"
            />

            <MyTextInput 
                label="Your Phone"
                name= "Phone"
                type="text"
                placeholder="*** ** ** ** ***"
            />
            
            <button type="submit" className='form__submit'>Checkout</button>
            </Form>
        </Formik>
    )
}

export default PayForm