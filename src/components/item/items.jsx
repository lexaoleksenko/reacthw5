import React from 'react';
import Item from './item';
import { useEffect, useContext} from 'react';
import { useSelector, useDispatch} from 'react-redux';

import { itemLoad } from '../../redux/actions';

import { ItemsContext } from '../mainPage/MainPage';

function Items(props){
    const dispatch = useDispatch();

    const loadItems = useSelector(state =>{
        // console.log("loadItems in useSelector>>>>>", state)
        const {itemLoadReducer} = state;
        return itemLoadReducer.itemLoad
    })

    useEffect(()=>{
        dispatch(itemLoad())    
    }, [])

    const itemsDisplay = useContext(ItemsContext)

    return (
    <>
        <div className={itemsDisplay}>
            {!!loadItems.length && loadItems.map(el => {
                return  <Item key={el.article} item={el} itemMain={"Yes"}/>
            })}
        </div>
    </>
    )
}

export default Items