import ReactDOM from 'react-dom/client';
import Modal from '../../modal/mod';

import { createStore, compose, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { rootReducer } from '../../../redux/rootReducer';

import {act} from "react-dom/test-utils"

let container = null;
let root = null; 

beforeEach(()=>{
    container = document.createElement("div");
    document.body.append(container);
    root = ReactDOM.createRoot(container);
});

afterEach(()=>{
    container.remove();
    container = null;
    root = null;
});

test("Modal test", ()=>{
    act(()=>{
        const store = createStore(rootReducer, compose(applyMiddleware(thunk)))
        root.render(
            <Provider store={store}>
                <Modal />
            </Provider>
        )
    })

    const el = document.querySelector('.modal')

    expect(el).toBeDefined()
    expect(el.style.display).toBe("none")
    expect(el.outerHTML).toMatchSnapshot()
})