import ReactDOM from 'react-dom/client';
import ButtonModEnt from '../../modal/buttonModEnt';

import {act} from "react-dom/test-utils"

let container = null;
let root = null; 

beforeEach(()=>{
    container = document.createElement("div");
    document.body.append(container);
    root = ReactDOM.createRoot(container);
});

afterEach(()=>{
    container.remove();
    container = null;
    root = null;
});

test("Button Enter test", ()=>{
    act(()=>{
        root.render(<ButtonModEnt />)
    })

    const el = document.querySelector("button")

    expect(el).toBeDefined()
    expect(el.textContent).toBe("Enter");
    expect(el.style.backgroundColor).toBe("rgb(105, 105, 105)");
    expect(el.style.color).toBe("rgb(255, 255, 255)");
    expect(el.outerHTML).toMatchSnapshot()
})