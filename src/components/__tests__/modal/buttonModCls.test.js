import ReactDOM from 'react-dom/client';
import ButtonModCls from "../../modal/buttonModCls";

import {act} from "react-dom/test-utils"

let container = null;
let root = null; 

beforeEach(()=>{
    container = document.createElement("div");
    document.body.append(container);
    root = ReactDOM.createRoot(container);
});

afterEach(()=>{
    container.remove();
    container = null;
    root = null;
});

test("Button Close test", ()=>{
    act(()=>{
        root.render(<ButtonModCls />)
    })

    const el = document.querySelector('button')

    expect(el).toBeDefined()
    expect(el.textContent).toBe("Cancel");
    expect(el.outerHTML).toMatchSnapshot()
})