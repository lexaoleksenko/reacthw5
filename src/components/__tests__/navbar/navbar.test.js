import ReactDOM from 'react-dom/client';
import Navbar from '../../navbar/navbar';

import { createStore, compose, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { rootReducer } from '../../../redux/rootReducer';

import {BrowserRouter} from "react-router-dom"

import {act} from "react-dom/test-utils"

let container = null;
let root = null; 

beforeEach(()=>{
    container = document.createElement("div");
    document.body.append(container);
    root = ReactDOM.createRoot(container);
});

afterEach(()=>{
    container.remove();
    container = null;
    root = null;
});

test("Navbar test", ()=>{
    act(()=>{
        const store = createStore(rootReducer, compose(applyMiddleware(thunk)))
        root.render(
            <Provider store={store}>
                <BrowserRouter>
                    <Navbar />
                </BrowserRouter>
            </Provider>
        )
    })

    const el = document.querySelector('.navbar')

    expect(el).toBeDefined()
    expect(el.outerHTML).toMatchSnapshot()
})