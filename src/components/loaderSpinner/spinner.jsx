import React from "react";
import { RotatingLines } from "react-loader-spinner";

import {useSelector } from "react-redux"


const Spin = (props) =>{
    const spinner = useSelector(state => state.appReducer.loading);
    // console.log("SPINNER>>>>", spinner)
    return(
    <>
        <div className="loader" style={{display: spinner ? "block" : "none"}}>
            <div className="loader_styles">
                <RotatingLines
                strokeColor="white"
                strokeWidth="5"
                animationDuration="0.75"
                width="96"
                visible={spinner}
                />
            </div>
            <div className="loader_bacgr">
            </div>
        </div>
    </>
        
    )
}

export default Spin