import React from 'react';
import {createContext} from 'react'

import Items from '../item/items';
import Modal from '../modal/mod';
import { useSelector} from 'react-redux';

export const ItemsContext = createContext()



function MainPage(props){

    const itemDisplay = useSelector(state =>{
        // console.log("useSelector>>>>>", state.itemsDispReducer)
        const {itemsReducer} = state;
        return itemsReducer.display
    })

    return (
    <>
        <div className='backgr'>
            <ItemsContext.Provider value={itemDisplay ? "items" : "itemsMod"}>
                <Items />
            </ItemsContext.Provider>
            <Modal modalMain={"Yes"}/>
        </div>
    </>
    )
}

export default MainPage