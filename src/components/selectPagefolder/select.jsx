import React from 'react';
import { useSelector } from 'react-redux';

import Item from '../item/item';

function Select(props){
    
    const selectItems = useSelector(state=>{
        // console.log("selectItems useSelector | STATE>>>>>", state)
        const {selectReducer} = state
        return selectReducer.itemsSelect
    })

    return(
        <>
            <div className='items'>
                {!!selectItems.length && selectItems.map(el => {
                    return <Item key={el.article} item={el} itemSelect="Yes"/>
                })}
            </div>
        </>
    )
}
export default Select