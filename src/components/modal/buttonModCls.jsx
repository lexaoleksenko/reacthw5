import React from 'react';
import PropTypes from 'prop-types';

function ButtonModCls(props){
    return (
        <button onClick={props.onCloseClick}  className='btnMod' >Cancel</button>
    )
}

ButtonModCls.propTypes = {
    onCloseClick: PropTypes.func,
}


export default ButtonModCls