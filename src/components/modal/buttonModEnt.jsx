import React from 'react';
import PropTypes from 'prop-types';

function ButtonModEnt(props){
    return (
        <button onClick={props.onEnterClick}  className='btnMod' style={{backgroundColor: props.btnBgCol, color: props.btnCol}}>Enter</button>
    )
}

ButtonModEnt.propTypes = {
    onEnterClick: PropTypes.func,
    btnBgCol: PropTypes.string,
    btnCol: PropTypes.string,
}

ButtonModEnt.defaultProps = {
    btnBgCol: "#696969",
    btnCol: "#FFFFFF",
}

export default ButtonModEnt