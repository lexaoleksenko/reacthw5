import React from 'react'
import './reset.css'
import './App.scss';

import { Routes, Route} from 'react-router-dom';

import Navbar from './components/navbar/navbar';
import MainPage from './components/mainPage/MainPage';
import CartPage from './components/cartPagefolder/сartPage';
import SelectPage from './components/selectPagefolder/selectPage';
import Spin from './components/loaderSpinner/spinner';

function App(props){
  return(
      <>
        <Navbar />
        <Spin />
        <Routes>
          <Route path='/' element={<MainPage />}/>
          <Route path='/cartPage' element={<CartPage />}/>
          <Route path='/selectPage' element={<SelectPage />}/>
        </Routes>
      </>
    )
}

export default App;