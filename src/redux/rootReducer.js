import { combineReducers } from "redux";
import { modalReducer } from "./reducers/modalReducer";
import { itemLoadReducer } from "./reducers/itemLoadReducer";
import { cartReducer } from "./reducers/cartReducer";
import { selectReducer } from "./reducers/selectReducer";
import { appReducer } from "./reducers/appReducer";
import { itemsReducer } from "./reducers/itemsDispReducer";

export const rootReducer = combineReducers({
    modalReducer,
    itemLoadReducer,
    cartReducer,
    selectReducer,
    appReducer,
    itemsReducer
})