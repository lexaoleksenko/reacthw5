import { TOGGLE_DISPLAY } from "../types";

const initialState = {
    display: true,
}
export const itemsReducer = (state = initialState, action) => {
    // console.log("INPUT TEXT REDUCER >>>","action>>",action,"state>>",state)
    switch(action && action.type){
        case TOGGLE_DISPLAY:
            const {display} = state
            return {
                ...state,
                display: !display 
            }
        default:
            return state;
    }
}