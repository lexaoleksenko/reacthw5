import {
    ITEM_ADD_CART,
    ITEMS_ADD_CART,
    ITEM_DEL_CART,
    DELETE_ITEM_CARD,
    DELETE_ITEM_ADD,
    DELETE_ITEM_ID,
    BUY_ITEM
} from "../types"

const initialState = {
    itemCart: {},
    itemsCart:[],
    deleteItemId: {},
}

export const cartReducer = (state = initialState, action) =>{
    // console.log("cartReducer| STATE>>>",state,"|  ACTION>>>",action)

    switch(action && action.type){
        case ITEM_ADD_CART:
            const itemNew = action.item 
            return {
                ...state,
                itemCart: itemNew
            }
        case ITEM_DEL_CART:
            return {
                ...state,
                itemCart: {}
            }
        case ITEMS_ADD_CART:
            const {itemCart} = state
            localStorage.setItem(`cartItem${itemCart.article}`, JSON.stringify(itemCart));
            return {
                ...state,
                itemsCart: [...state.itemsCart, itemCart]
            }
        case DELETE_ITEM_ADD:
            const {id} = action

            return{
                ...state,
                deleteItemId: id
            }
        case DELETE_ITEM_ID:
            return{
                ...state,
                deleteItemId: {}
            }
        case DELETE_ITEM_CARD:
            return (()=>{
                const {deleteItemId} = state;
                localStorage.removeItem(`cartItem${deleteItemId}`);
                const {itemsCart} = state;
                const itemIndex = itemsCart.findIndex(res => res.article === deleteItemId)

                const newItemsCart = [
                    ...itemsCart.slice(0,itemIndex),
                    ...itemsCart.slice(itemIndex +1)
                ];
                return {
                    ...state,
                    itemsCart: newItemsCart
                }
            })()
        case BUY_ITEM:
            const {itemsCart} = state;
            itemsCart.forEach(e => {
                localStorage.removeItem(`cartItem${e.article}`);
            });
            return{
                ...state,
                itemsCart:[]
            }

        default:
            return state;
    }
}