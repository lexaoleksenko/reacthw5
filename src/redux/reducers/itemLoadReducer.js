import {
    ITEM_LOAD,
    STAR_STATE_TRUE,
    STAR_STATE_FALSE,
} from "../types"

const initialState = {
    itemLoad: [],
    starState: [],
}

export const itemLoadReducer = (state = initialState, action) =>{
    // console.log("itemLoadReducer| STATE>>>",state,"|  ACTION>>>",action)

    switch(action && action.type){
        case ITEM_LOAD:
            // const itm = action && action.data && action.data.products ? action.data.products : [{title:"1",id:"1",img:"",price:"1"},{title:"2",id:"2",img:"",price:"2"}];
            const itemNew = action.data.products.map(el => {
                return{
                    name: el.title,
                    article: el.id,
                    img: "./phph.jpg",
                    price: el.price,
                }
            })
            return {
                ...state,
                itemLoad: itemNew
            }
        case STAR_STATE_TRUE:
            return (()=>{
                const {article} = action;
                const {itemLoad} = state;
                const newItem = itemLoad.find(el => el.article === article)
                localStorage.setItem(`starState${newItem.article}`, JSON.stringify(newItem));
                const newStarState = {
                    article: newItem.article
                    };
                return {
                    ...state,
                    starState: [...state.starState, newStarState]
                }
            })();
        case STAR_STATE_FALSE:
            return (()=>{
                const {article} = action;
                localStorage.removeItem(`starState${article}`);
                const {starState} = state;
                const itemIndex = starState.findIndex(res => res.article === article)
                const newStarState = [
                    ...starState.slice(0,itemIndex),
                    ...starState.slice(itemIndex +1)
                ];

                return {
                    ...state,
                    starState: newStarState
                }
            })();
        default:
            return state;
    }
}

