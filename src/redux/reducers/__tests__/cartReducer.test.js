import { cartReducer, initialState } from "../cartReducer";

describe("cartReduser should work", ()=>{
    test("Should return \"initialState\" ", ()=>{
        expect(cartReducer()).toEqual({...initialState, deleteItemId: {}, itemCart: {}, itemsCart: []})
    });
    test("Should return \"itemCart\" ", ()=>{
        expect(cartReducer(initialState, {type: "ITEM_ADD_CART", item:{name:"1",article:"1",img:"./phph.jpg",price:"1"}})).toEqual({...initialState, deleteItemId: {}, itemCart: {name:"1",article:"1",img:"./phph.jpg",price:"1"}, itemsCart: []})
    });
    test("Should return \"delete itemCart\" ", ()=>{
        expect(cartReducer({...initialState, deleteItemId: {}, itemCart: {name:"1",article:"1",img:"./phph.jpg",price:"1"}, itemsCart: []}, {type: "ITEM_DEL_CART"})).toEqual({...initialState, deleteItemId: {}, itemCart: {}, itemsCart: []})
    });
})