import { appReducer, initialState } from "../appReducer";


describe("appReduser should work", ()=>{
    test("Should return \"display: true\" ", ()=>{
        expect(appReducer(initialState, {type: "LOADER_DISPLAY_ON"})).toEqual({...initialState, loading: true})
    });
    test("Should return \"display: false\" ", ()=>{
        expect(appReducer(initialState, {type: "LOADER_DISPLAY_OF"})).toEqual({...initialState, loading: false})
    });
    test("Should return \"display: false\" ", ()=>{
        expect(appReducer()).toEqual({...initialState, loading: false})
    });
})