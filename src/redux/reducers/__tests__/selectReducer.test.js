import { selectReducer, initialState } from "../selectReducer";

describe("selectReduser should work", ()=>{
    test("Should return \"initialState\" ", ()=>{
        expect(selectReducer()).toEqual({...initialState, itemsSelect: []})
    });
    test("Should return \"itemsSelect\" ", ()=>{
        expect(selectReducer(initialState, {type: "ITEMS_ADD_SELECT", item: {name:"1",article:"1",img:"./phph.jpg",price:"1"}})).toEqual({...initialState, itemsSelect: [{name:"1",article:"1",img:"./phph.jpg",price:"1"}]})
    });
    test("Should return \"delete itemsSelect\" ", ()=>{
        expect(selectReducer({...initialState, itemsSelect: [{name:"1",article:"1",img:"./phph.jpg",price:"1"}]},{type: "ITEM_DEL_SELECT"})).toEqual({...initialState, itemsSelect: []})
    });
})