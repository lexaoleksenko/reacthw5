import { itemsReducer, initialState } from "../itemsDispReducer";

describe("itemsDispReducer should work", ()=>{
    test("Should return \"display: false\" ", ()=>{
        expect(itemsReducer(initialState, {type: "TOGGLE_DISPLAY"})).toEqual({...initialState, display: false})
    });
    test("Should return \"display: true\" ", ()=>{
        expect(itemsReducer()).toEqual({...initialState, display: true})
    });
})