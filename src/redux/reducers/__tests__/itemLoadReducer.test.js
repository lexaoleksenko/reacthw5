import {itemLoadReducer, initialState} from "../itemLoadReducer";

describe("itemLoadReduser should work", ()=>{
    test("Should return \"initialState\" ", ()=>{
        expect(itemLoadReducer()).toEqual({...initialState, itemLoad: [], starState: []})
    });
    test("Should return \"itemLoad\" ", ()=>{
        expect(itemLoadReducer(initialState, {type: "ITEM_LOAD", data: {products: [{title:"1",id:"1",img:"",price:"1"},{title:"2",id:"2",img:"",price:"2"}]}})).toEqual({...initialState, itemLoad: [{name:"1",article:"1",img:"./phph.jpg",price:"1"},{name:"2",article:"2",img:"./phph.jpg",price:"2"}], starState: []})
    });
})