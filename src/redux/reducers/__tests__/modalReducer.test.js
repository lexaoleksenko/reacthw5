import { modalReducer, initialState } from "../modalReducer";

describe("modalReducer should work", ()=>{
    test("Should return \"activeModal: true\" ", ()=>{
        expect(modalReducer(initialState, {type: "TOGGLE_MODAL"})).toEqual({...initialState, activeModal: true})
    });
    test("Should return \"activeModal: false\" ", ()=>{
        expect(modalReducer()).toEqual({...initialState, activeModal: false})
    });
})