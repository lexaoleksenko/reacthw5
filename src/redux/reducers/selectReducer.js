import {
    ITEMS_ADD_SELECT,
    ITEM_DEL_SELECT,
} from "../types"

const initialState = {
    itemsSelect: [],
}

export const selectReducer = (state = initialState, action) =>{
    // console.log("selectReducer| STATE>>>",state,"|  ACTION>>>",action)

    switch(action && action.type){
        case ITEMS_ADD_SELECT:
            const itemNew = action.item
            return {
                ...state,
                itemsSelect: [...state.itemsSelect, itemNew]
            }
        case ITEM_DEL_SELECT:
            return (()=>{
                const article = action && action.article ? action.article: "1";
                const {itemsSelect} = state;
                const itemIndex = itemsSelect.findIndex(res => res.article === article)

                const newItemsSelect = [
                    ...itemsSelect.slice(0,itemIndex),
                    ...itemsSelect.slice(itemIndex +1)
                ];
                return {
                    ...state,
                    itemsSelect: newItemsSelect,
                }
            })();
        default:
            return state;
    }
}